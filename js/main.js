$(document).ready(function () {

    $(".main-slider-init").on('init', function (e) {
        if ($('.main-slide').length === 1) {
            $('.main-slider-init .slick-dots').addClass('hidden')
        }
    });

    $(".main-slider-init").slick({
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: false,
        nextArrow: false
    })

    

    $(".product-slider-init").slick({
        dots: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        prevArrow: false,
        nextArrow: false,
        autoplay: true,
        autoplaySpeed: 4000,
        infinite: true,
        dots: false,
        responsive: [{
            breakpoint: 1000,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
            }
        }]
    });

    $('.burger').click(function (e) {
        $(this).toggleClass('active');
        $('.logo').toggleClass('active');
        $('.menu').toggleClass('active');

        $('body').toggleClass('mob_scroll_false');
    })

    $('.close').click(function (e) {
        $('.burger').removeClass('active');
        $('.logo').removeClass('active');
        $('.menu').removeClass('active');

        $('body').removeClass('mob_scroll_false');
    })


    


    if ($(window).width() < 1000){
        $('.footer-bot-left').append($('.socials.socials_footer'))
    }

    $('.accordion__item').click(function (e) {
        $(this).toggleClass('active')
    })

    

});

( function() {

    var youtube = document.querySelectorAll( ".youtube" );
    
    for (var i = 0; i < youtube.length; i++) {
        
        var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/sddefault.jpg";
        
        var image = new Image();
                image.src = source;
                image.addEventListener( "load", function() {
                    youtube[ i ].appendChild( image );
                }( i ) );
        
                youtube[i].addEventListener( "click", function() {

                    var iframe = document.createElement( "iframe" );

                            iframe.setAttribute( "frameborder", "0" );
                            iframe.setAttribute( "allowfullscreen", "" );
                            iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );

                            this.innerHTML = "";
                            this.appendChild( iframe );
                } );    
    };
    
} )();